<?php

namespace Drupal\yandex_market_xml\plugins;

/**
 * Interface of currency plugin.
 *
 * Provides currencies info for different commerce systems.
 */
interface CurrencyInterface {

  /**
   * Plugin title.
   *
   * @return string
   *   Plugin title.
   */
  public static function title();

  /**
   * Get all active currencies.
   *
   * @return array
   *   Key is identifier, value is array with rate element.
   */
  public static function currencies();

  /**
   * Get default currency.
   *
   * @return string
   *   Default currency identifier.
   */
  public static function defaultCurrency();

}
