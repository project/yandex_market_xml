<?php

namespace Drupal\yandex_market_xml\plugins;

/**
 * Drupal commerce currency plugin, depends on drupal commerce module.
 */
class Commerce implements CurrencyInterface {

  /**
   * Plugin title.
   *
   * @return string
   *   Plugin title.
   */
  public static function title() {
    return t('Commerce currencies');
  }

  /**
   * Get all active currencies.
   *
   * @return array
   *   Key is identifier, value is array which contains rate element.
   */
  public static function currencies() {
    return commerce_currencies(TRUE);
  }

  /**
   * Get default currency.
   *
   * @return string
   *   Default currency identifier.
   */
  public static function defaultCurrency() {
    return commerce_default_currency();
  }

}
