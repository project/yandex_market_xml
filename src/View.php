<?php

namespace Drupal\yandex_market_xml;

/**
 * Class of view which provides products XML for offers element of YML.
 */
class View extends Item {

  /**
   * Get all views displays for settings form.
   *
   * @see https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7
   *
   * @return array
   *   Views displays with keys
   *    "view name" -> "view and display with ' ' delimiter".
   *   Value is view and display with ' ' delimiter.
   */
  public function all() {
    $aViews = views_get_all_views();
    $aViewsOptions = array();
    foreach ($aViews as $sName => $aView) {
      if (!isset($aViewsOptions[$sName])) {
        $aViewsOptions[$sName] = array();
      }
      foreach ($aView->display as $sDisplay => $aDisplay) {
        $aViewsOptions[$sName][$sName . ' ' . $sDisplay] = $sName . ' ' . $sDisplay;
      }
    }
    return $aViewsOptions;
  }

  /**
   * Check if view display is correct.
   *
   * @param string $pvName
   *   View and display with ' ' delimiter.
   *
   * @return bool
   *   Check result.
   */
  public function check($pvName) {
    return TRUE;
  }

  /**
   * Get offers XML.
   *
   * @return \DOMDocument
   *   Document or boolean false on error.
   */
  public function xml() {
    $vResult = FALSE;
    do {
      $sView = $this->get();
      if (is_null($sView)) {
        break;
      }
      $aViewParams = explode(' ', $sView);
      if (count($aViewParams) <> 2) {
        break;
      }

      global $language;
      $aLanguages = language_list();
      $language = $aLanguages['en'];

      $sOffers = views_embed_view($aViewParams[0], $aViewParams[1]);
      $oOffersDocument = new \DOMDocument();
      $oOffersDocument->loadXML($sOffers);
      foreach ($oOffersDocument->childNodes as $oOffers) {
        foreach ($oOffers->childNodes as $oOffer) {
          if($oOffer->nodeName != '#text') {
            $this->processOffer($oOffer);
          }
        }
      }
      $vResult = $oOffersDocument;

      // $language = $oLanguage;.
    } while (0);
    return $vResult;
  }

  /**
   * Process offer xml - move id, type, available, bid tags to offer attribute.
   */
  protected function processOffer(&$poOffer) {
    $aRemove = array();
    foreach ($poOffer->childNodes as $oProperty) {
      if (in_array($oProperty->nodeName, array('id', 'type', 'available', 'bid'))) {
        $poOffer->setAttribute($oProperty->nodeName, $oProperty->nodeValue);
        $aRemove[] = $oProperty;
      }
    }
    foreach ($aRemove as $oRemoveNode) {
      $poOffer->removeChild($oRemoveNode);
    }
    return $poOffer;
  }

}
